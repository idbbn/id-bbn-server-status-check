import pymongo
from pymongo.errors import ServerSelectionTimeoutError
import datetime
import smtplib
from email.message import EmailMessage
import os
import settings

def log(to_log):
    to_log = str(to_log)
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " : " + to_log)

def send_email(target, source, subject, body):
    log("Sending Mail")

    msg = EmailMessage()

    msg.set_content(body)

    msg['To'] = target
    msg['From'] = source
    msg['Subject'] = subject

    s = smtplib.SMTP('localhost')
    s.send_message(msg)

    log("Possible response:")

    print(s)

    s.quit()

def mongodb():
    try:
        client = pymongo.MongoClient("localhost", 27017)
        info = client.server_info()
        log("connection made to mongo db.")
        log(info)
    except pymongo.errors.ServerSelectionTimeoutError as err:

        date_now = datetime.datetime.now()

        print(date_now)

        from_field = settings.sender
        subject = "ikirouta MongoDB down!"

        addresses = settings.addresses
        for address in addresses:
            send_email(address, from_field, subject, str(err))
            log("Message sent to: " + address)

def nginx():
    try:
        response = os.system("systemctl is-active nginx")
        log("nginx ikirouta is active with status "+ str(response))

    except OSError as err:

        date_now = datetime.datetime.now()

        print(date_now)

        from_field = settings.sender
        subject = "ikirouta nginx down"

        addresses = settings.addresses
        for address in addresses:
            send_email(address, from_field, subject, str(err))
            log("Message sent to: " + address)

if __name__ == '__main__':
    log("Starting Execution")
    mongodb()
    nginx()
